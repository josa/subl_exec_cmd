import sublime, sublime_plugin, os, re


    

class ExecCmdCommand(sublime_plugin.WindowCommand):



    
    active_file = ''
    active_folders = ''
    cmds = {}

    def run(self):
        self.cmds = self.settings_get("commands", {})

        available_cmds = []

        self.file_name = self.window.active_view().file_name()
        
        folders = self.window.folders()
        if len(folders) >= 1:
            self.active_folder = folders[0]

        for cmd in self.cmds.keys():
            cmd_data = self.cmds[cmd]
            

            if 'match_file' in cmd_data:

                p = re.compile(cmd_data['match_file'])
                if self.file_name and p.match(self.file_name):
                    available_cmds.append(cmd)
            
            if 'match_folder' in cmd_data:

                p = re.compile(cmd_data['match_folder'])
                if self.active_folder and p.match(self.active_folder):
                    available_cmds.append(cmd)

            else:
                available_cmds.append(cmd)


        if len(available_cmds) > 0:
            self.show_select(available_cmds, self.on_select_cmd)


    def on_select_cmd(self, index, value):

        cmd_data = self.cmds[value]
        exec_cmd = cmd_data['cmd'];
        
        if isinstance(exec_cmd, list):
            for exec_str in exec_cmd:
                self.exec_cmd(exec_str)

        else:
            self.exec_cmd(exec_str)


    def exec_cmd(self, exec_cmd):

        if self.file_name:
            exec_cmd = exec_cmd.replace('<file>', '"' + self.file_name + '"')
            
        if self.active_folder:
            exec_cmd = exec_cmd.replace('<folder>', '"' + self.active_folder + '"')
        
        exec_result = os.system(exec_cmd)


        








    input = None
    input_callback = None
    select_options = None
    select_callback = None

    
    def show_select(self, options, callback = None):
        self.select_callback = callback
        self.select_options = options
        self.select = self.window.show_quick_panel(self.select_options, self._on_select_done)

    def _on_select_done(self, index):
        value = self.select_options[index]
        self.on_select_done(index, value)

    def on_select_done(self, index, value):
        if self.select_callback != None:
            self.select_callback(index, value)

    def show_input(self, label = "", default = "", callback = None):
        
        self.input_callback = callback
        self.input = self.window.show_input_panel(
            label,
            default,
            self.on_input_done,
            self.on_input_change,
            self.on_input_cancel
        )

    def on_input_done(self, value):
        if self.select_callback != None:
            self.select_callback(index, value)
        pass

    def on_input_change(self, value):
        pass

    def on_input_cancel(self):
        pass


    def settings_set(self, key, value):
        settings = sublime.load_settings('Exec.sublime-settings')
        settings.set(key, value)
        sublime.save_settings('Exec.sublime-settings')

        # print key + ' = ' + value

    def settings_get(self, value, default = None):
        settings = sublime.load_settings('Exec.sublime-settings')
        return settings.get(value, default)





